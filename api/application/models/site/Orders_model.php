<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model {

	public function get( $userId )
	{

		$this->db->where( 'userId' , $userId );
		$q = $this->db->get( 'orders' );
		$q = $q->result();

		return $q;

	}

	public function create( $data, $itemsArray )
	{
		$this->db->insert( 'orders' , $data );
        $length = count($itemsArray);
        for ($x = 0; $x < $length; $x++) {
            $this->db->where( 'name' , $itemsArray[$x]['name'] );
            $quantity = $itemsArray[$x]['quantity'];

            $q = $this->getSold($itemsArray[$x]['name']);
            $array = json_decode(json_encode($q), True);

            $onstock = $array[0]['onstock'] - $quantity;
            $quantity = $quantity + $array[0]['sold'];

            $sold = array('onstock' => $onstock, 'sold' => $quantity);

            $this->db->where( 'name' , $itemsArray[$x]['name'] );
            $this->db->update( 'products' , $sold );
        }
	}

	public function getSold ( $name ){
        $this->db->where( 'name' , $name );
        $q = $this->db->get( 'products' );
        $q = $q->result();
        return $q;
    }

}

/* End of file  */
/* Location: ./application/models/ */