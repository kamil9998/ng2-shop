<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model {

	public function get( $id = false )
	{

		if ( $id == false )
		{
			$q = $this->db->get( 'products' );
			$q = $q->result();
		}
		else
		{
			$this->db->where( 'id' , $id );
			$q = $this->db->get( 'products' );
			$q = $q->row();
		}

		return $q;

	}

    public function getBest()
    {

        $this->db->order_by('sold', 'desc');
        $this->db->limit(1);
        $q = $this->db->get('products')->row();

        return $q;

    }

    public function getNew()
    {

        $this->db->order_by('id', 'desc');
        $this->db->limit(3);
        $q = $this->db->get('products')->result();

        return $q;

    }

}

/* End of file  */
/* Location: ./application/models/ */