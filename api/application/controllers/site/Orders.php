<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.poczta.onet.pl',
            'smtp_port' => 465,
            'smtp_user' => 'kamil9998@vp.pl',
            'smtp_pass' => '12gru12Tat',
            'mailtype'  => 'text',
            'charset'   => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'site/Orders_model' );

	}

	public function create()
	{

		$data = $_POST;

		$items = $this->input->post( 'items' );
        $itemsArray = $items;
		$items = json_encode( $items );

		$data['items'] = $items;

		$this->Orders_model->create( $data, $itemsArray );

        echo json_encode( 'Dodano zamówienie' );


        $this->email->from('kamil9998@vp.pl', 'Angular 2 Shop');
        $this->email->to('k.wojt@imagine.am');
        $this->email->subject('Złożenie zamówienia w sklepie Angular 2 Shop');

        $message = "Dziękujemy za złożenia zamówienia w naszym sklepie.\r\n";
        $message .= "Oto lista zakupów:\r\n";
        foreach ($itemsArray as $value) {
            print_r($value);
            $message .= $value['name'];
            $message .= ' x'.$value['quantity'];
            $message .= ' - '.$value['price'].' zł';
            $message .= "\r\n";
        }



        $this->email->message($message);
        $this->email->send();


	}

	public function get()
	{
		$payload = $this->input->post( 'payload' );
		$userId = $payload['userId'];

		$output = $this->Orders_model->get( $userId );
		echo json_encode( $output );
	}

}

/* End of file Orders.php */
/* Location: ./application/controllers/Orders.php */