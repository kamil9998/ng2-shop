<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'site/User_model' );	

	}

	public function get( $id )
	{
		$output = $this->User_model->get( $id );
        echo json_encode( $output );
	}

    public function update()
    {
        $user = $_POST;
        $id = $user['id'];
        $original_value = $this->User_model->get( $id );
        $this->form_validation->set_error_delimiters( '' , '' );
        if($user['name'] != $original_value->name ) {
            $is_unique_name = 'is_unique[users.name]';
        } else {
            $is_unique_name =  'required';
        }
        if($user['email'] != $original_value->email ) {
            $is_unique_email = 'is_unique[users.email]';
        } else {
            $is_unique_email =  'required';
        }

        $this->form_validation->set_rules('name', 'Name', "{$is_unique_name}");
        $this->form_validation->set_rules( 'email' , 'Email', "$is_unique_email");

        if ( $this->form_validation->run() ) {
            unset($user['confirmPassword']);
            $user['password'] = crypt($user['password'], config_item('encryption_key'));
            unset($user['id']);
            $this->User_model->update($user, $id);
            $errors['success'] = true;
            echo json_encode($errors);
        }else{
            $errors['role'] = form_error( 'role' );
            $errors['name'] = form_error( 'name' );
            if( strlen($errors['name']) <4 ){
                $errors['name'] = '';
            }

            $errors['email'] = form_error( 'email' );
            if( strlen($errors['email']) <4 ){
                $errors['email'] = '';
            }
            $errors['success'] = false;
            echo json_encode( $errors );
        }

    }

	public function create()
	{

		$this->form_validation->set_error_delimiters( '' , '' );
		$this->form_validation->set_rules( 'name' , 'Name' , 'is_unique[users.name]' );
		$this->form_validation->set_rules( 'email' , 'Email' , 'is_unique[users.email]' );

		if ( $this->form_validation->run() )
		{
			$user = $_POST;
			$user['role'] = 'user';
			unset( $user['confirmPassword'] );
			$user['password'] = crypt( $user['password'] , config_item( 'encryption_key' ) );
			$errors['debug'] = $user['password'];
			$this->User_model->create( $user );
			$errors['success'] = true;
			echo json_encode( $errors );
		}
		else
		{
			$errors['name'] = form_error( 'name' );
			$errors['email'] = form_error( 'email' );
			$errors['success'] = false;
			echo json_encode( $errors );
		}

	}

	public function login()
	{
		$email = $this->input->post( 'email' );
		$password = $this->input->post( 'password' );
		$password = crypt( $password , config_item( 'encryption_key' ) );
		$login = $this->User_model->login( $email , $password );

		if ( !$login )
		{
			$output['error'] = 'Błędne hasło lub email';
		}
		else
		{
			$token = $this->jwt->encode( array(
				'userId' => $login->id,
				'name' => $login->name,
				'email' => $login->email,
				'role' => $login->role
				) , config_item( 'encryption_key' ) );

			$output['token'] = $token;
		}

		echo json_encode( $output );

	}

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */