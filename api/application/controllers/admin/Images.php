<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $post = file_get_contents( 'php://input' );
        $_POST = json_decode( $post , true );

		$this->load->model( 'admin/Products_model' );
	}

	public function upload( $id )
	{
		if ( !empty( $_FILES ) )
		{
		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

		    $basePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
		    $basePath = $basePath . $id . DIRECTORY_SEPARATOR;

            if (!file_exists($basePath)) {
                mkdir( $basePath , 0700 );
            }


		    $uploadPath = $basePath . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPath );

		    $answer = array( 'answer' => 'File transfer completed' );

		    $json = json_encode( $answer );
		    echo $json;

		}
		else
		{
		    echo 'No files';
		}

	}

	public function get( $id )
	{
	    $basePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
	    $basePath = $basePath . $id . DIRECTORY_SEPARATOR;

	    if ( !is_dir( $basePath ) ) {
            echo json_encode('Brak zdjęć');
            return;
        }

	    $files = scandir( $basePath );
	    $files = array_diff( $files , array( '.' , '..' ) );

	    $newFiles = array();
	    foreach ( $files as $file )
	    {
	    	$newFiles[] .= $file;
	    }
	    echo json_encode( $newFiles );

	}

	public function delete()
	{

        $postImage = $_POST;

		$id = $postImage['id'];
		$image = $postImage['image'];

	    $imagePath = FCPATH . '..' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
	    $imagePath = $imagePath . $id . DIRECTORY_SEPARATOR;
	    $imagePath = $imagePath . $image;
        chown($imagePath,666);
	    unlink( $imagePath );
        echo json_encode('Usunięto zdjęcie');
	}

	public function setThumbnail()
	{

		$input = $_POST;
		$productId = $input['id'];

		$imageName = $input['thumb'];
		$product['thumb'] = $imageName;

		$this->Products_model->setThumb( $productId , $product );
        echo json_encode('Ustawiono miniaturkę');
	}

}
