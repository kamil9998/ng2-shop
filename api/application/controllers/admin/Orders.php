<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.poczta.onet.pl',
            'smtp_port' => 465,
            'smtp_user' => 'kamil9998@vp.pl',
            'smtp_pass' => '12gru12Tat',
            'mailtype'  => 'text',
            'charset'   => 'utf-8'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'admin/Orders_model' );

		$token = $this->input->post( 'token' );
		$token = $this->jwt->decode( $token , config_item( 'encryption_key' ) );	

		if ( $token->role != 'admin' )
			exit( 'Nie jesteś adminem' );
	}

	public function get( $id = false )
	{
		$output = $this->Orders_model->get( $id );
		echo json_encode( $output );
	}

	public function update( $id )
	{
		$data['status'] = $this->input->post( 'status' );

		$this->Orders_model->update( $id , $data );

        echo json_encode( 'Zmieniono status zamówienia' );

        $this->email->from('kamil9998@vp.pl', 'Angular 2 Shop');
        $this->email->to('k.wojt@imagine.am');
        $this->email->subject('Wysyłka zamówienia w sklepie Angular 2 Shop');

        $message = "Twoje zamówienie numer ";
        $message .= $id;
        $message .= " zostało wysłane.\r\n";
        $message .= "Dziękujemy za zakupy w naszym sklepie i zapraszamy ponownie.\r\n";




        $this->email->message($message);
        $this->email->send();
	}

	public function delete( $id )
	{
		$this->Orders_model->delete( $id );

        echo json_encode( 'Usunięto zamówienie' );
	}

}

/* End of file Orders.php */
/* Location: ./application/controllers/Orders.php */