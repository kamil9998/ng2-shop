import { Component, ViewEncapsulation } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { HeaderComponent } from "./shared/header.component";
import {FooterComponent} from "./shared/footer.component";
import {AuthService} from "./shared/auth.service";
import {LocalStorageService} from "angular2-localstorage/LocalStorageEmitter";



@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [HeaderComponent, FooterComponent, ROUTER_DIRECTIVES],
  providers: [AuthService, LocalStorageService],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  constructor(private storageService: LocalStorageService){}
}
