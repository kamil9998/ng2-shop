import { provideRouter } from "@angular/router";

import { SignupComponent } from "./user/signup.component";
import { SigninComponent } from "./user/signin.component";
import { AuthGuard } from "./shared/auth.guard";
import {UsersComponent} from "./protected/users.component";
import { USERS_ROUTES } from "./protected/users.routes";
import {HomeComponent} from "./shared/home.component";
import {PRODUCTS_ROUTES} from "./product/products.routes";
import {ProductsComponent} from "./product/products.component";
import {CartComponent} from "./product/cart/cart.component";
import {CheckoutComponent} from "./product/checkout/checkout.component";
import {MyUserEditComponent} from "./user/my-user-edit.component";
import {OrdersComponent} from "./orders/orders.component";
import {ORDERS_ROUTES} from "./orders/orders.routes";

export const APP_ROUTES = [
  provideRouter([
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'signin', component: SigninComponent },
    { path: 'useredit', component: MyUserEditComponent },
    { path: 'cart', component: CartComponent },
    { path: 'checkout', component: CheckoutComponent },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard], children: USERS_ROUTES },
    { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard], children: ORDERS_ROUTES },
    { path: 'products', component: ProductsComponent, children: PRODUCTS_ROUTES }
  ])
];
