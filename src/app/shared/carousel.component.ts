import {Component} from '@angular/core';
import {Image} from './image.interface';

@Component({
    moduleId: module.id,
    selector: 'css-carousel',
    template: `
 <div class="carousel">

  <ul class="slides">

    <a href=""><li *ngFor="let image of images">
      <h2>{{image.title}}</h2>
      <img src="{{image.url}}" alt="">
    </li></a>

  </ul>

</div>
  `,
    styles: [`
.carousel{
    overflow:hidden;
    width:100%;
}
.slides{
    list-style:none;
    position:relative;
    width:300%; /* Number of panes * 100% */
    overflow:hidden; /* Clear floats */
        /* Slide effect Animations*/
    -moz-animation:carousel 20s infinite;
    -webkit-animation:carousel 20s infinite;
    animation:carousel 20s infinite;
}
.slides li{
    position:relative;
    float:left;
    width: 33.33333%; /* 100 / number of panes */
}
.carousel img{
    display:block;
    width:100%;
    max-width:100%;
}
.carousel h2{
    margin-bottom: 0;
    font-size:1em;
    padding:1.5em 0.5em 1.5em 0.5em;
    position:absolute;
    right:0px;
    bottom:0px;
    left:0px;
    text-align:center;
    color:#fff;
    background-color:rgba(0,0,0,0.75);
    text-transform: uppercase;
}

@keyframes carousel{
    0%    { left:-2%; }
    23%   { left:-2%; }
    25% { left:-101.5%; }
    48% { left:-101.5%; }
    50%   { left:-200%; }
    73%   { left:-200%; }
    75% { left:-101.5%; }
    98% { left:-101.5%; }
    100%  { left:-2%; }
}
  `],
})
export class CarouselComponent {
    public images = IMAGES;
}

var IMAGES: Image[] = [
    { "title": "Rowery Szosowe", "url": "../images/slider/slider.jpg" },
    { "title": "Rowery MTB", "url": "../images/slider/slider2.jpg" },
    { "title": "Rowery Triathlonowe", "url": "../images/slider/slider31.jpg" },
];