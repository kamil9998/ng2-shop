import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import {CarouselComponent} from "./carousel.component";
import {ProductService} from "./product.service";
import { ROUTER_DIRECTIVES, Router } from "@angular/router";
import {Product} from "./product.interface";

@Component({
    moduleId: module.id,
    selector: 'my-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css','../app.component.css'],
    directives: [ROUTER_DIRECTIVES, CarouselComponent],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit{
    constructor(private router: Router, private productService: ProductService){}

    private new: Product[];
    private best: Product;

    ngOnInit(): any {
        this.productService.getNewProduct().subscribe(
            (data: Product[]) => {
                this.new = data;
                console.log(this.new);
            }
        );
        this.productService.getBestProduct().subscribe(
            (data: Product) => {
                this.best = data;
                console.log(this.best);
            }
        );
    }

    private route( id ){
        this.router.navigate(['/products', id]);
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}