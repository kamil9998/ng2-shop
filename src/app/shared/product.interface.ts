export interface Product {
    id?: number;
    name: string;
    category: string;
    onstock: number;
    size: string;
    price: number;
    color: string;
    description: string;
    thumb: string;
    token?: string;
}