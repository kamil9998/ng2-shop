export interface Cart {
    name: string;
    price: number;
    thumb: string;
    quantity: number;
}