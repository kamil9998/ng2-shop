import { Injectable } from "@angular/core";
import {Subject} from "rxjs/Subject";

import { Router } from "@angular/router";
import {Headers, Http, Response} from "@angular/http";
import 'rxjs/Rx';
import {Product} from "./product.interface";
import {AuthHttp, AuthConfig, AUTH_PROVIDERS, JwtHelper} from "angular2-jwt/angular2-jwt";
import {Cart} from "./cart.interface";
import {LocalStorage, SessionStorage} from "angular2-localstorage/WebStorage";
import {Order} from "./order.interface";



@Injectable()
export class ProductService {
    constructor(private router: Router, private http: Http) {}

    private products: Product[] = [];
    @LocalStorage() public cart: Array<Cart> = [];
    @LocalStorage() public cartSum: any = 0;
    private jwtHelper: JwtHelper = new JwtHelper();



    private sumCartChange = new Subject<boolean>();
    sumCartChange$ = this.sumCartChange.asObservable();

    setSumCartChange(changeStatus: boolean){
        console.log(changeStatus);
        this.sumCartChange.next(changeStatus);
    }

    @SessionStorage() public minPrice:number = 0;
    @SessionStorage() public maxPrice:number = 0;


    private dataChange = new Subject<boolean>();
    dataChange$ = this.dataChange.asObservable();
    setDataChange(changeStatus: boolean){
        this.dataChange.next(changeStatus);
    }



    addToCart(product: Product, qty) {

        const newProduct = { name:'',price:0,thumb:"",quantity: parseInt(qty) };
        if ( !this.cart.length )
        {
            newProduct.name = product.name;
            newProduct.price = product.price;
            newProduct.thumb = product.thumb;
            this.cart.push(newProduct);
        }
        else {
            var addNew = true;

            for (var i = 0; i < this.cart.length; i++) {
                if (product.name == this.cart[i].name) {
                    addNew = false;
                    this.cart[i].quantity = this.cart[i].quantity + parseInt(qty);
                }
            }
            if (addNew) {
                newProduct.name = product.name;
                newProduct.price = product.price;
                newProduct.thumb = product.thumb;
                this.cart.push(newProduct);
            }
        }
    }

    getProducts() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('../api/site/products/get').map((data: Response) => data.json());
    }

    getProduct(id: Number) {
        const body = '{"id": '+id+'}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/site/products/get/', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    getBestProduct() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('../api/site/products/getBest').map((data: Response) => data.json());
    }

    getNewProduct() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('../api/site/products/getNew').map((data: Response) => data.json());
    }

    editProduct(product: Product) {
        product.token = localStorage.getItem('token');
        const body = JSON.stringify(product);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/products/update/', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    createProduct(product: Product) {
        product.token = localStorage.getItem('token');
        const body = JSON.stringify(product);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/products/create/', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    deleteProduct(product: Product) {
        product.token = localStorage.getItem('token');
        const body = JSON.stringify(product);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/products/delete', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    getImages(id: Number){
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('../api/admin/images/get/'+id).map((data: Response) => data.json());
    }

    deleteImage(name: string, id: number){

        const body = '{"image": "'+name+'","id": "'+id+'"}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/images/delete', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    setThumbnail(product: Product){
        const body = JSON.stringify(product);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/images/setThumbnail', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    createOrder(order: Order){
        order.status = 0;
        const body = JSON.stringify(order);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/site/orders/create/', body, {
            headers: headers
        }).map((data: Response) => data);
    }

    clearCart(){
        this.cart = [];
        this.setDataChange(true);
    }

    getDataOrders() {
        const body = '{"token": "'+localStorage.getItem('token')+'"}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/orders/get', body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    getOrder(id: Number) {
        const body = '{"token": "'+localStorage.getItem('token')+'"}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/orders/get/'+id, body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    changeStatus(id: Number) {
        const body = '{"token": "'+localStorage.getItem('token')+'","status": "1"}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/orders/update/'+id, body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    deleteOrder(id: Number) {
        console.log(id);
        const body = '{"token": "'+localStorage.getItem('token')+'"}';
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('../api/admin/orders/delete/'+id, body, {
            headers: headers
        }).map((data: Response) => data.json());
    }

    isAdmin(): boolean {

        var token = localStorage.getItem('token');
        if(token !== null)
            return this.jwtHelper.decodeToken(token).role == 'admin';
        else
            return false;
    }

}
