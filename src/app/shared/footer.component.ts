import { Component } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'my-footer',
    templateUrl: 'footer.component.html',
    styleUrls: ['footer.component.css','../app.component.css'],
    directives: [ROUTER_DIRECTIVES]
})
export class FooterComponent {

}
