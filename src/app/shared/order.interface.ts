import {Cart} from "./cart.interface";
export interface Order {
    id?: number;
    userId: number;
    name: string;
    surname: string;
    street: string;
    city: string;
    zip: string;
    phone: string;
    email: string;
    items: Cart[];
    total: string;
    status: number;
    token: string;
}