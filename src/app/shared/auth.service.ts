import { Injectable, EventEmitter} from "@angular/core";

import { User } from "./user.interface";
import { Router } from "@angular/router";
import {Headers, Http, Response} from "@angular/http";
import 'rxjs/Rx';
import {Subject} from "rxjs/Subject";

import {AuthHttp, AuthConfig, AUTH_PROVIDERS, JwtHelper} from "angular2-jwt/angular2-jwt";


@Injectable()
export class AuthService {
  constructor(private router: Router, private http: Http) {}

  private userLoggedOut = new EventEmitter<any>();
  private jwtHelper: JwtHelper = new JwtHelper();

  private users: User[] = [];

  private dataChange = new Subject<boolean>();
  dataChange$ = this.dataChange.asObservable();

  setDataChange(changeStatus: boolean){
    this.dataChange.next(changeStatus);
  }

  signupUser(user: User){
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/site/user/create/', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  signupUserAdmin(user: User){
    user.token = localStorage.getItem('token');
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/admin/users/create/', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  signinUser(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/site/user/login/', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }
  
  getDataUsers() {
    const body = '{"token": "'+localStorage.getItem('token')+'"}';
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/admin/users/get', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  getUser(id: Number) {
    const body = '{"token": "'+localStorage.getItem('token')+'"}';
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/admin/users/get/'+id, body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  getUserWithoutAuth(id: Number) {
    const body = '{"id": "'+id+'"}';
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/site/user/get/'+id, body, {
      headers: headers
    }).map((data: Response) => data.json());

  }

  editUser(user: User) {
    user.token = localStorage.getItem('token');
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/admin/users/update/', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  editUserWithoutAuth(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/site/user/update/', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  deleteUser(user: User) {
    user.token = localStorage.getItem('token');
    const body = JSON.stringify(user);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    console.log(user);
    //noinspection TypeScriptUnresolvedFunction
    return this.http.post('../api/admin/users/delete', body, {
      headers: headers
    }).map((data: Response) => data.json());
  }

  logout() {
    localStorage.removeItem('token');
    this.userLoggedOut.emit(null);
    this.router.navigate(['/signin']);
  }

  logoutWithoutRoute() {
    localStorage.removeItem('token');
    this.userLoggedOut.emit(null);
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }
  
  isAdmin(): boolean {

    var token = localStorage.getItem('token');
    if(token !== null)
      return this.jwtHelper.decodeToken(token).role == 'admin';
    else
      return false;
  }

  getId(): number {
    var token = localStorage.getItem('token');
    if(token !== null)
      return this.jwtHelper.decodeToken(token).userId;

  }

  getLogin(): string {
    var token = localStorage.getItem('token');
    if(token !== null)
      return this.jwtHelper.decodeToken(token).name;
  }

  getEmail(): string {
    var token = localStorage.getItem('token');
    if(token !== null)
      return this.jwtHelper.decodeToken(token).email;
  }
}
