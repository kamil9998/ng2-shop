import { Component, OnInit } from "@angular/core";
import { ROUTER_DIRECTIVES, Router } from "@angular/router";

import { AuthService } from "./auth.service";
import {HeaderSigninComponent} from "../user/header-signin.component";
import {SmallCartComponent} from "../product/cart/small-cart.component";
import {ProductService} from "./product.service";

@Component({
    moduleId: module.id,
    selector: 'my-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.css','../app.component.css'],
  directives: [ROUTER_DIRECTIVES, HeaderSigninComponent, SmallCartComponent]
})
export class HeaderComponent implements OnInit{
  constructor(private authService: AuthService, private  productService: ProductService, private router: Router) {
      productService.sumCartChange$.subscribe(
          value => {if(value) this.cartUpdate();console.log('updateCart');}
      );
  }

  private cartI: number;
    private cartSum: any;
    private login = 'Moje konto';

    ngOnInit(): any {
        this.cartUpdate();
        this.getLogin();
    }

    cartUpdate(){
        this.cartI = this.productService.cart.length;
        this.cartSum = this.productService.cartSum;
    }
  isAdmin() {
    return this.authService.isAdmin();
  }
  isAuth() {
    return this.authService.isAuthenticated();
  }

  onLogout() {
    this.authService.logout();
  }

    onCheckout() {
        if(this.isAuth()){
            this.router.navigate(['checkout/']);
        }
        else{
            this.router.navigate(['signup/']);
        }
    }

    getLogin() {
        this.login = this.authService.getLogin();
    }
}
