import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ProductService} from "../../shared/product.service";
import { Router } from "@angular/router";


@Component({
    moduleId: module.id,
    selector: "my-small-cart",
    template: `
        <!--Mini cart-->
        
        <ul *ngIf="cart.length > 0" class="cart-inner">
            <li class="mini-cart-content" *ngFor="let cartItem of cart; let i = index">
                <div class="mini-cart-img"><img src="{{cartItem.thumb}}" alt="product search one"></div>
                <div class="mini-cart-ds">
                    <h6><a href="single-product.html">{{cartItem.name}}</a></h6>
                    <span class="mini-cart-meta">
                        {{cartItem.price }} zł
                        <span class="mini-meta">
                           <span class="mini-qty">Ilość: {{cartItem.quantity}}</span>
                        </span>
                    </span>
                </div>
                <a (click)="deleteProduct(i)"><span class="mini-cart-delete"><img src="../images/delete.png" alt="delete"></span></a>
            </li>
            
            <li class="mini-footer">
                <a class="view-cart">Koszyk</a>
                <a class="check-out">Zapłać</a>
            </li>
        </ul>
        <!--End mini cart-->
    `,
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['../../shared/header.component.css','../../app.component.css']
})
export class SmallCartComponent implements OnInit{

    constructor(private productService: ProductService, private router: Router) {}

    private cart;
    private cartSum = 0;

    ngOnInit(): any {
        this.cart = this.productService.cart;
    }

    deleteProduct(index: number){
        this.productService.cart.splice(index,1);
        this.updateSum();
    }
    navigateBack(){
        this.router.navigate(['cart/']);
    }

    updateSum(){
        this.cartSum = 0;
        for(var i=0; i<this.productService.cart.length; i++) {
            this.cartSum = this.cartSum + this.productService.cart[i].price * this.productService.cart[i].quantity;
        }
        this.productService.cartSum = this.cartSum;
        this.productService.setSumCartChange(true);
        this.productService.setDataChange(true);
    }
}