import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../shared/product.service";
import { Router } from "@angular/router";


@Component({
    moduleId: module.id,
    selector: "my-cart",
    templateUrl: 'cart.component.html',
    styleUrls: ['cart.component.css', '../../app.component.css'],
})
export class CartComponent implements OnInit{

    constructor(private productService: ProductService, private router: Router) {}

    private cart;
    private cartSum = 0;

    ngOnInit(): any {
        this.cart = this.productService.cart;
        this.updateSum();
    }

    deleteProduct(index: number){
        this.productService.cart.splice(index,1);
        this.updateSum();
    }
    navigateBack(){
        this.router.navigate(['products/']);
    }

    updateSum(){
        this.cartSum = 0;
        for(var i=0; i<this.productService.cart.length; i++) {
            this.cartSum = this.cartSum + this.productService.cart[i].price * this.productService.cart[i].quantity;
        }
        this.productService.cartSum = this.cartSum;

        this.productService.setSumCartChange(true);
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}