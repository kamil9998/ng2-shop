import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import {Product} from "../../shared/product.interface";
import {ProductService} from "../../shared/product.service";
import {ImageZoom} from 'angular2-image-zoom';



@Component({
    moduleId: module.id,
    selector: 'my-product-detail',
    templateUrl: 'product-detail.component.html',
    styleUrls: ['product-detail.component.css', '../../app.component.css'],
    directives: [ImageZoom]
})
export class ProductDetailComponent implements OnInit, OnDestroy {
    selectedProduct: Product;
    private productIndex: number;
    private subscription: Subscription;
    private images: string[];
    private colors: string[] = [];
    private cartSum = 0;
    private cart;

    constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService) {}

    ngOnInit(): any {

        this.cart = this.productService.cart;

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                this.productIndex = params['id'];
                this.productService.getProduct(this.productIndex).subscribe(
                    (data: Product) => {
                        this.selectedProduct = data;
                        this.colors = this.selectedProduct.color.split(',');
                    }
                );
            }
        );
        this.getImages();

    }

    addToCart(qty:number) {
        this.productService.addToCart(this.selectedProduct,qty);
        console.log('update');
        this.updateSum();
    }

    onEdit() {
        this.router.navigate(['/products', this.productIndex, 'edit']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    updateSum(){
        this.cartSum = 0;
        for(var i=0; i<this.productService.cart.length; i++) {
            this.cartSum = this.cartSum + this.productService.cart[i].price * this.productService.cart[i].quantity;
        }
        this.productService.cartSum = this.cartSum;
        this.productService.setSumCartChange(true);
    }

    getImages(){
        this.productService.getImages(this.productIndex)
            .subscribe( (data: string[]) => {
                if (data instanceof Array) {
                    this.images = data;
                    for (var i = 0; i < this.images.length; i++)
                    {
                        this.images[i] = '../uploads/'+this.productIndex+'/'+this.images[i];
                    }
                } else {
                    this.images = null;
                }
            });
    }

    /*ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }*/

}
