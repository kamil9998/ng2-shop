import { Component } from '@angular/core';
import {ProductFilterComponent} from "./product-filter/product-filter.component";
import {ProductListComponent} from "./product-list/product-list.component";

@Component({
    moduleId: module.id,
    template: `
        <div class="tz-shop">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <my-product-filter></my-product-filter>
                    </div>
                    <div class="col-md-9">
                        <my-product-list></my-product-list>
                    </div>
                </div>
            </div>
        </div>
    `,
    styles: [],
    directives: [ProductFilterComponent, ProductListComponent]
})
export class ProductStartComponent {
}