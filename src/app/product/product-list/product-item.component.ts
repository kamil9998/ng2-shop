import { Component, Input, ViewEncapsulation, OnInit, ViewContainerRef} from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from "@angular/router";

import {Product} from "../../shared/product.interface";
import {ProductService} from "../../shared/product.service";
import {AuthService} from "../../shared/auth.service";

@Component({
    moduleId: module.id,
    selector: 'my-product-item',
    templateUrl: 'product-item.component.html',
    styleUrls: ['product-item.component.css','product-list.component.css', '../../app.component.css'],
    directives: [ROUTER_DIRECTIVES],
    encapsulation: ViewEncapsulation.None
})
export class ProductItemComponent implements OnInit{
    @Input() product: Product;
    @Input() productId: number;

    private colors: string[] = [];
    private cartSum = 0;

    constructor(private router: Router, private productService: ProductService, private authService: AuthService, viewContainer: ViewContainerRef) {
    }

    ngOnInit(): any {
        this.colors = this.product.color.split(',');
    }

    onDelete() {
        this.productService.deleteProduct(this.product).subscribe(
            data => {
                this.productService.setDataChange(true);
            }
        );

    }

    onEdit() {
        this.router.navigate(['/products', this.product.id, 'edit']);
    }

    addToCart() {
        this.productService.addToCart(this.product,1);
        this.updateSum();
/*
        this.openAlert();
*/
    }

    isAdmin(){
        return this.authService.isAdmin();
    }

    updateSum(){
        this.cartSum = 0;
        for(var i=0; i<this.productService.cart.length; i++) {
            this.cartSum = this.cartSum + this.productService.cart[i].price * this.productService.cart[i].quantity;
        }
        this.productService.cartSum = this.cartSum;
        this.productService.setSumCartChange(true);
        this.productService.setDataChange(true);
    }

    /*openAlert() {
        return this.modal.alert()
            .title('Hello World')
            .body('In Angular 2')
            .open();
    }*/
}