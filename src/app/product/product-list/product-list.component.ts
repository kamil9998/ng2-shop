import { Component, OnInit } from '@angular/core';
import {Product} from "../../shared/product.interface";
import {ProductService} from "../../shared/product.service";
import {ProductItemComponent} from "./product-item.component";
import {ROUTER_DIRECTIVES, Router} from "@angular/router";
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';
import {PricePipe} from "../product-filter/pipe/price.pipe";


@Component({
    moduleId: module.id,
    selector: "my-product-list",
    templateUrl: 'product-list.component.html',
    styleUrls: ['product-list.component.css', '../../app.component.css'],
    directives: [ROUTER_DIRECTIVES ,ProductItemComponent,PaginationControlsCmp],
    pipes: [PaginatePipe, PricePipe],
    providers: [PaginationService]
})
export class ProductListComponent implements OnInit{
    products: Product[] = [];
    perPageOption = [12, 6, 3];
    perPageSelected = 12;
    listView = false;

    priceSliderValue:number = 0;
    anotherPriceSliderValue: number = 0;


    constructor(private productService: ProductService, private router: Router) {
        productService.dataChange$.subscribe(
            value => {if(value) this.getProducts();console.log('dsa');}
        );
    }

    changeView(b: boolean) {
        this.listView = b;
    }

    changePerPage(n: number){
        this.perPageSelected = n;
    }

    ngOnInit(): any {
        this.getProducts();
        var cart = this.productService.cart;
        this.priceSliderValue = this.productService.minPrice;
        this.anotherPriceSliderValue = this.productService.maxPrice;
    }

    getProducts(){
        //noinspection TypeScriptUnresolvedFunction
        this.productService.getProducts().subscribe(
            (data: Product[]) => {
                this.products = data;
            }
        );
    }

    addProduct(){
        this.router.navigate(['/products', 'new']);
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}