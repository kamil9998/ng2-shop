import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'PricePipe',
    pure: false
})
export class PricePipe implements PipeTransform{

    transform(input:any, arg1: number, arg2: number) {
        let minPrice = arg1;
        let maxPrice = arg2;
        return input.filter(products => {
            return parseFloat(products.price) >= +minPrice && parseFloat(products.price) <= +maxPrice;
        });
    }

}