import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../shared/product.service";

@Component({
    moduleId: module.id,
    selector: "my-product-filter",
    templateUrl: 'product-filter.component.html',
    styleUrls: ['product-filter.component.css', '../../app.component.css'],
    directives: []
})
export class ProductFilterComponent implements OnInit{

    constructor(private productService: ProductService){}

    priceSliderValue:number = 0;
    anotherPriceSliderValue: number = 10000;

    ngOnInit():any {
        this.productService.minPrice = this.priceSliderValue;
        this.productService.maxPrice = this.anotherPriceSliderValue;
    }

}
