import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass, NgStyle} from '@angular/common';
import {FileSelectDirective, FileDropDirective, FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload/ng2-file-upload';
import { Subscription } from "rxjs/Rx";
import { ActivatedRoute, Router } from "@angular/router";


@Component({
    moduleId: module.id,
    selector: 'my-image-upload',
    templateUrl: 'image-upload.component.html',
    directives: [FILE_UPLOAD_DIRECTIVES, NgClass, NgStyle, CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class ImageUploadComponent implements OnInit {

    constructor(private route: ActivatedRoute) {}
    private subscription: Subscription;
    private productIndex: number;
    public uploader:FileUploader;
    public hasBaseDropZoneOver:boolean = false;
    public hasAnotherDropZoneOver:boolean = false;
    @Output() notifyUpload: EventEmitter<any> = new EventEmitter();

    sentNotifyUpload(){
        this.notifyUpload.emit(true);
    }

    public fileOverBase(e:any):void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e:any):void {
        this.hasAnotherDropZoneOver = e;
    }
    ngOnInit(): any {

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                this.productIndex = +params['id'];
                const URL = '../api/admin/images/upload/' + this.productIndex;;

                this.uploader = new FileUploader({url: URL});

            }
        );

    }






}
