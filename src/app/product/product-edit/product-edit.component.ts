import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import {
    FormArray,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder,
    REACTIVE_FORM_DIRECTIVES
} from "@angular/forms";
import {Product} from "../../shared/product.interface";
import {ProductService} from "../../shared/product.service";
import {ImageUploadComponent} from "../image-upload/image-upload.component";



@Component({
    moduleId: module.id,
    selector: 'rb-product-edit',
    templateUrl: 'product-edit.component.html',
    styleUrls: ['../product-detail/product-detail.component.css', '../../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES, ImageUploadComponent]
})
export class ProductEditComponent implements OnInit, OnDestroy {
    productForm: FormGroup;

    error = false;
    errorMessage = '';
    errors: any;
    private productIndex: number;
    private product: Product;
    private subscription: Subscription;
    private formLoaded = false;
    private images: string[];
    private isNew = true;


    constructor(private route: ActivatedRoute,
                private productService: ProductService,
                private formBuilder: FormBuilder,
                private router: Router) {}

    ngOnInit() {

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if (params.hasOwnProperty('id')) {
                    this.isNew = false;
                    this.productIndex = +params['id'];
                    //noinspection TypeScriptUnresolvedFunction
                    this.productService.getProduct(this.productIndex).subscribe(
                        data => {
                            this.product = data;
                            this.formLoaded = true;
                        }
                    );
                } else {
                    this.isNew = true;
                    this.product = null;
                }
            }

        );
        this.initForm(this.formLoaded);
        this.getImages();
    }

    onSubmit() {




        const newProduct = this.productForm.value;
        newProduct.id = this.productIndex;
        if (this.isNew) {
            this.productService.createProduct(newProduct).subscribe(
                data => {
                    this.navigateBack();
                    this.productService.setDataChange(true);
                }
            );
        } else {
            this.productService.editProduct(newProduct).subscribe(
                data => {
                    this.navigateBack();
                    this.productService.setDataChange(true);
                }
            );
        }
    }

    onCancel() {
        this.navigateBack();
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    uploadImages(b: boolean){
        if(b){
            this.getImages();
        }
    }

    getImages(){
        this.productService.getImages(this.productIndex)
            .subscribe( (data: string[]) => {
                if (data instanceof Array) {
                    this.images = data;
                } else {
                    this.images = null;
                }
            });
    }

    setAsThumbnail(image: string){
        this.product.thumb = '../uploads/'+this.productIndex+'/'+image;
        console.log(this.product);
        this.productService.setThumbnail(this.product).subscribe( data => { console.log(data) });
    }

    deleteImage(image: string){
        this.productService.deleteImage(image, this.productIndex)
            .subscribe( data => console.log(data));
        this.ngOnInit();
    }

    private navigateBack() {
        this.router.navigate(['products/']);
    }

    private initForm(formLoaded: boolean) {
        let productName = '';
        let productCategory = '';
        let productOnstock = 0;
        let productSize = '';
        let productPrice = 0;
        let productColor = '';
        let productDescription = '';


        if (!this.isNew && formLoaded) {
            productName = this.product.name;
            productCategory = this.product.category;
            productOnstock = this.product.onstock;
            productSize = this.product.size;
            productPrice = this.product.price;
            productColor = this.product.color;
            productDescription = this.product.description;
        }
        this.productForm = this.formBuilder.group({
            name: [productName, Validators.required],
            category: [productCategory, Validators.required],
            onstock: [productOnstock, Validators.required],
            size: [productSize, Validators.required],
            price: [productPrice, Validators.required],
            color: [productColor, Validators.required],
            description: [productDescription, Validators.required]
        });

    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }

}
