import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../shared/product.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormControl, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";
import {Order} from "../../shared/order.interface";
import {AuthService} from "../../shared/auth.service";



@Component({
    moduleId: module.id,
    selector: "my-checkout",
    templateUrl: 'checkout.component.html',
    styleUrls: ['checkout.component.css', '../../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES]

})
export class CheckoutComponent implements OnInit{

    constructor(private productService: ProductService, private router: Router, private fb: FormBuilder,private authService: AuthService) {}

    private cart;
    private cartSum = 0;
    checkoutForm: FormGroup;
    private order: Order;
    errors: any;
    dataSuccess: boolean = false;

    ngOnInit(): any {
        this.cart = this.productService.cart;
        this.updateSum();

        this.checkoutForm = this.fb.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            street: ['', Validators.required],
            city: ['', Validators.required],
            zip: ['', Validators.required],
            phone: ['', Validators.required]
        });
    }

    onPay() {
        this.order = this.checkoutForm.value;
        this.order.userId = this.authService.getId();
        this.order.items = this.cart;
        this.order.email = this.authService.getEmail();
        this.order.total = this.cartSum.toFixed(2).toString().replace('.',',');
        this.productService.createOrder(this.order).subscribe(
            data => {
                this.dataSuccess = true;
                this.productService.clearCart();
                this.productService.setSumCartChange(true);
            }
        );
    }


    updateSum(){
        this.cartSum = 0;
        for(var i=0; i<this.productService.cart.length; i++) {
            this.cartSum = this.cartSum + this.productService.cart[i].price * this.productService.cart[i].quantity;
        }
        this.productService.cartSum = this.cartSum;

        this.productService.setSumCartChange(true);
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}