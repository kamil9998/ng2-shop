import { RouterConfig } from "@angular/router";
import {ProductStartComponent} from "./product-start.component";
import {ProductDetailComponent} from "./product-detail/product-detail.component";
import {ProductEditComponent} from "./product-edit/product-edit.component";


export const PRODUCTS_ROUTES: RouterConfig = [
    { path: '', component: ProductStartComponent },
    { path: 'new', component: ProductEditComponent },
    { path: ':id', component: ProductDetailComponent },
    { path: ':id/edit', component: ProductEditComponent }
];