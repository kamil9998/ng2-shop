import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router";
import {UsersListComponent} from "./users-list/users-list.component";
import {AuthService} from "../shared/auth.service";

@Component({
  moduleId: module.id,
  selector: 'rb-users',
  templateUrl: 'users.component.html',
  directives: [UsersListComponent, ROUTER_DIRECTIVES]
})
export class UsersComponent{
}
