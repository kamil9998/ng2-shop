import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import {
    FormArray,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder,
    REACTIVE_FORM_DIRECTIVES
} from "@angular/forms";
import {User} from "../../shared/user.interface";
import {AuthService} from "../../shared/auth.service";


@Component({
    moduleId: module.id,
    selector: 'rb-user-edit',
    templateUrl: 'user-edit.component.html',
    styles: [],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class UserEditComponent implements OnInit, OnDestroy {
    userForm: FormGroup;

    error = false;
    errorMessage = '';
    errors: any;
    errorName: string;
    errorEmail: string;
    private userIndex: number;
    private user: User;
    private isNew = true;
    private subscription: Subscription;
    private formLoaded = false;


    constructor(private route: ActivatedRoute,
                private authService: AuthService,
                private formBuilder: FormBuilder,
                private router: Router) {}

    ngOnInit(): any {

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if (params.hasOwnProperty('id')) {
                    this.isNew = false;
                    this.userIndex = +params['id'];
                    //noinspection TypeScriptUnresolvedFunction
                    this.authService.getUser(this.userIndex).subscribe(
                        data => {
                            this.user = data;
                            this.formLoaded = true;
                        }
                    );
                } else {
                    this.isNew = true;
                    this.user = null;
                }

            }
        );
        this.initForm(this.formLoaded);
    }

    onSubmit() {
        const newUser = this.userForm.value;
        newUser.id = this.userIndex;
        if (this.isNew) {
            this.authService.signupUserAdmin(this.userForm.value).subscribe(
                data => {
                    this.errors = data;
                    if( !this.errors.success ){
                        this.errorName = this.errors.name;
                        this.errorEmail = this.errors.email;
                    }
                    else{
                        this.errorName = "";
                        this.errorEmail = "";
                        this.navigateBack();
                        this.authService.setDataChange(true);

                    }
                }
            );
        } else {
            this.authService.editUser(newUser).subscribe(
                data => {
                    this.errors = data;
                    if( !this.errors.success ){
                        this.errorName = this.errors.name;
                        this.errorEmail = this.errors.email;
                    }
                    else{
                        this.errorName = "";
                        this.errorEmail = "";
                        this.navigateBack();
                        this.authService.setDataChange(true);

                    }
                }
            );
        }
    }

    onCancel() {
        this.navigateBack();
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private navigateBack() {
        this.router.navigate(['users/']);
    }

    private initForm(formLoaded: boolean) {
        let userName = '';
        let userEmail = '';
        let userRole = '';

        if (!this.isNew && formLoaded) {
            userName = this.user.name;
            userEmail = this.user.email;
            userRole = this.user.role;
        }
        this.userForm = this.formBuilder.group({
            name: [userName, Validators.required],
            email: [userEmail, Validators.compose([
                Validators.required,
                this.isEmail
            ])],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.compose([
                Validators.required,
                this.isEqualPassword.bind(this)
            ])],
            role: [userRole, Validators.required]
        });
    }

    isEmail(control: FormControl): {[s: string]: boolean} {
        if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
            return {noEmail: true};
        }
    }

    isEqualPassword(control: FormControl): {[s: string]: boolean} {
        if (!this.userForm) {
            return {passwordsNotMatch: true};

        }
        if (control.value !== this.userForm.controls['password'].value) {
            return {passwordsNotMatch: true};
        }
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }

}
