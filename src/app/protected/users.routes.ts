import { RouterConfig } from "@angular/router";

import { UsersStartComponent } from "./users-start.component";
import {UserDetailComponent} from "./users-detail/user-detail.component";
import {UserEditComponent} from "./user-edit/user-edit.component";

export const USERS_ROUTES: RouterConfig = [
    { path: '', component: UsersStartComponent },
    { path: 'new', component: UserEditComponent },
    { path: ':id', component: UserDetailComponent },
    { path: ':id/edit', component: UserEditComponent }
];
