import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";

import {User} from "../../shared/user.interface";
import {AuthService} from "../../shared/auth.service";


@Component({
    moduleId: module.id,
    selector: 'rb-user-detail',
    templateUrl: 'user-detail.component.html'
})
export class UserDetailComponent implements OnInit, OnDestroy {
    selectedUser: User;
    private userIndex: number;
    private subscription: Subscription;

    constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService) {}

    ngOnInit(): any {

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                this.userIndex = params['id'];
                this.authService.getUser(this.userIndex).subscribe(
                    (data: User) => {
                        this.selectedUser = data;
                    }
                );
            }
        );

    }

    onEdit() {
        this.router.navigate(['/users', this.userIndex, 'edit']);
    }

    onDelete() {
        this.authService.deleteUser(this.selectedUser).subscribe(
            data => {
                console.log(data);
            }
        );
        this.router.navigate(['/users']);
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }

}
