import {Component, OnInit} from "@angular/core";
import { AuthService } from "../../shared/auth.service";

import {User} from "../../shared/user.interface";
import {ROUTER_DIRECTIVES} from "@angular/router";
import {UserItemComponent} from "./user-item.component";
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';


@Component({
    moduleId: module.id,
    selector: 'rb-users-list',
    templateUrl: 'users-list.component.html',
    directives: [UserItemComponent, ROUTER_DIRECTIVES, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService, UserItemComponent]

})
export class UsersListComponent implements OnInit {
    users: User[] = [];

    constructor(private authService: AuthService) {
        authService.dataChange$.subscribe(
            value => {if(value) this.getUsers();}
        );
    }

    ngOnInit(): any {

        this.getUsers();
    }

    getNotification(evt) {
        if(evt){
            this.getUsers();
            console.log('Pobrano użytkowników');
        }
    }


    getUsers() {
        //noinspection TypeScriptUnresolvedFunction
        this.authService.getDataUsers().subscribe(
            (data: User[]) => {
                this.users = data;
            }
        );

    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}
