import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ROUTER_DIRECTIVES, Router} from "@angular/router";

import { User } from '../../shared/user.interface';
import {AuthService} from "../../shared/auth.service";

@Component({
  moduleId: module.id,
  selector: 'rb-user-item',
  templateUrl: 'user-item.component.html',
  directives: [ROUTER_DIRECTIVES]
})
export class UserItemComponent {
  @Input() user: User;
  @Input() userId: number;
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private authService: AuthService) {}


    sendInfo(){
        this.notifyParent.emit(true);
    }

  onDelete() {
    this.authService.deleteUser(this.user).subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/users']);
          this.sendInfo();
        }
    );

  }
}