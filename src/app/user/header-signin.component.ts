import {Component, OnInit} from "@angular/core";
import { FormBuilder, FormGroup, Validators, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";

import { AuthService } from "../shared/auth.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'my-login',
    template: `
        <form [formGroup]="myForm" (ngSubmit)="onSignin()">
            <p class="form-content">
                <label for="email">Email</label>
                <input formControlName="email" type="email" id="email" #email class="form-control">
            </p>
            <p class="form-content">
                <label for="password">Password</label>
                <input formControlName="password" type="password" id="password" class="form-control">
            </p>
            <span class="error">
                {{errorMessage}}
            </span>
            <p class="form-footer">
                <button type="submit" [disabled]="!myForm.valid" class="pull-right button_class">Zaloguj się</button>
            </p>
            
        </form>
    `,
    styleUrls: ['../shared/header.component.css','../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class HeaderSigninComponent implements OnInit {
    myForm:FormGroup;
    error = false;
    errorMessage:string;

    constructor(private fb:FormBuilder, private authService:AuthService, private router: Router) {
    }

    onSignin() {
        this.authService.signinUser(this.myForm.value)
            .subscribe(data => {
                if(data.token){
                    localStorage.setItem('token', data.token);
                    this.errorMessage = "";
                    this.router.navigate(['/home']);
                }
                else
                    this.errorMessage = data.error;
            });
    }

    ngOnInit():any {
        this.myForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required],
        });
    }
}