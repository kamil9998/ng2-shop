import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";

import { AuthService } from "../shared/auth.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    templateUrl: "signup.component.html",
    styleUrls: ['signup.component.css','../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class SignupComponent implements OnInit {
    myForm: FormGroup;
    error = false;
    errorMessage = '';
    errors: any;
    errorName: string;
    errorEmail: string;
    dataSuccess: boolean = false;

    constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {}

    onLogin() {
      this.router.navigate(['/signin']);
    }

    onSignup() {
        this.authService.signupUser(this.myForm.value).subscribe(
            data => {
                this.errors = data;
                if( !this.errors.success ){
                    this.errorName = this.errors.name;
                    this.errorEmail = this.errors.email;
                    console.log(this.errors.debug);
                }
                else{
                    this.errorName = "";
                    this.errorEmail = "";
                    this.dataSuccess = true;
                    console.log(this.errors.debug);
                }
            }
        );
    }

    ngOnInit(): any {
        this.myForm = this.fb.group({
            name: ['', Validators.required],
            email: ['', Validators.compose([
                Validators.required,
                this.isEmail
            ])],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.compose([
                Validators.required,
                this.isEqualPassword.bind(this)
            ])],
        });
    }

    isEmail(control: FormControl): {[s: string]: boolean} {
        if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
            return {noEmail: true};
        }
    }

    isEqualPassword(control: FormControl): {[s: string]: boolean} {
        if (!this.myForm) {
            return {passwordsNotMatch: true};

        }
        if (control.value !== this.myForm.controls['password'].value) {
            return {passwordsNotMatch: true};
        }
    }
}
