import {Component, OnInit} from "@angular/core";
import { FormBuilder, FormGroup, Validators, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";

import { AuthService } from "../shared/auth.service";
import {Router} from "@angular/router";


@Component({
    moduleId: module.id,
    templateUrl: "signin.component.html",
    styleUrls: ['signup.component.css','../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class SigninComponent implements OnInit {
    myForm:FormGroup;
    error = false;
    errorMessage:string;

    constructor(private fb:FormBuilder, private authService:AuthService, private router: Router) {
    }

    onSignin() {
        this.authService.signinUser(this.myForm.value)
            .subscribe(data => {
                 if(data.token){
                     localStorage.setItem('token', data.token);
                     this.errorMessage = "";
                     this.router.navigate(['/home']);
                 }
                 else
                    this.errorMessage = data.error;
             });
    }

    ngOnInit():any {
        this.myForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required],
        });
    }
}

