import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, REACTIVE_FORM_DIRECTIVES } from "@angular/forms";

import { AuthService } from "../shared/auth.service";
import {Router} from "@angular/router";
import {User} from "../shared/user.interface";

@Component({
    moduleId: module.id,
    templateUrl: "my-user-edit.component.html",
    styleUrls: ['signup.component.css','../app.component.css'],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class MyUserEditComponent implements OnInit {
    myForm: FormGroup;
    error = false;
    errorMessage = '';
    errors: any;
    errorName: string;
    errorEmail: string;
    dataSuccess: boolean = false;
    private userIndex: number;
    private user: User;
    private formLoaded = false;

    constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {}

    onLogin() {
        this.router.navigate(['/signin']);
    }

    ngOnInit(): any {

        this.userIndex = this.authService.getId();

        this.authService.getUserWithoutAuth(this.userIndex).subscribe(
            data => {
                console.log(data);
                this.user = data;
                this.formLoaded = true;

            }
        );
        this.initForm(this.formLoaded);
    }

    onSubmit() {
        const newUser = this.myForm.value;
        newUser.id = this.userIndex;
        this.authService.editUserWithoutAuth(newUser).subscribe(
            data => {
                this.errors = data;
                if( !this.errors.success ){
                    this.errorName = this.errors.name;
                    this.errorEmail = this.errors.email;
                }
                else{
                    this.errorName = "";
                    this.errorEmail = "";
                    this.dataSuccess = true;
                    this.authService.logoutWithoutRoute();
                }
            }
        );
    }

    private initForm(formLoaded: boolean) {
        let userName = '';
        let userEmail = '';

        if (formLoaded) {
            userName = this.user.name;
            userEmail = this.user.email;
        }

        this.myForm = this.fb.group({
            name: [userName, Validators.required],
            email: [userEmail, Validators.compose([
                Validators.required,
                this.isEmail
            ])],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.compose([
                Validators.required,
                this.isEqualPassword.bind(this)
            ])]
        });
    }

    isEmail(control: FormControl): {[s: string]: boolean} {
        if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
            return {noEmail: true};
        }
    }

    isEqualPassword(control: FormControl): {[s: string]: boolean} {
        if (!this.myForm) {
            return {passwordsNotMatch: true};

        }
        if (control.value !== this.myForm.controls['password'].value) {
            return {passwordsNotMatch: true};
        }
    }
}
