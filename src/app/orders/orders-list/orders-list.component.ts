import {Component, OnInit} from "@angular/core";

import {ROUTER_DIRECTIVES} from "@angular/router";
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-pagination';
import {Order} from "../../shared/order.interface";
import {ProductService} from "../../shared/product.service";
import {OrderItemComponent} from "./order-item.component";


@Component({
    moduleId: module.id,
    selector: 'rb-orders-list',
    templateUrl: 'orders-list.component.html',
    directives: [OrderItemComponent, ROUTER_DIRECTIVES, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService, OrderItemComponent]

})
export class OrdersListComponent implements OnInit {
    orders: Order[] = [];

    constructor(private productService: ProductService) {}

    ngOnInit(): any {

        this.getOrders();
    }

    getNotification(evt) {
        if(evt){
            this.getOrders();
            console.log('Pobrano użytkowników');
        }
    }


    getOrders() {
        //noinspection TypeScriptUnresolvedFunction
        this.productService.getDataOrders().subscribe(
            (data: Order[]) => {
                this.orders = data;
            }
        );

    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }
}