import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ROUTER_DIRECTIVES, Router} from "@angular/router";

import {ProductService} from "../../shared/product.service";
import {Order} from "../../shared/order.interface";


@Component({
    moduleId: module.id,
    selector: 'rb-order-item',
    templateUrl: 'order-item.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class OrderItemComponent {
    @Input() order: Order;
    @Input() orderId: number;
    @Output() notifyParent: EventEmitter<any> = new EventEmitter();

    constructor(private router: Router, private productService: ProductService) {}

    sendInfo(){
        this.notifyParent.emit(true);
    }

    onDelete() {
        this.productService.deleteOrder(this.order.id).subscribe(
            data => {
                console.log(data);
                this.router.navigate(['/orders']);
                this.sendInfo();
            }
        );

    }

    changeStatus(){
        this.productService.changeStatus(this.order.id).subscribe(
            data => {
                console.log(data);
                this.router.navigate(['/orders']);
                this.sendInfo();
            }
        );
    }
}