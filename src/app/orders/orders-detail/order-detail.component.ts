import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import {Order} from "../../shared/order.interface";
import {ProductService} from "../../shared/product.service";
import {Cart} from "../../shared/cart.interface";



@Component({
    moduleId: module.id,
    selector: 'rb-order-detail',
    templateUrl: 'order-detail.component.html'
})
export class OrderDetailComponent implements OnInit, OnDestroy {
    selectedOrder: Order;
    private orderIndex: number;
    private subscription: Subscription;
    private parser: string;
    private items: any;

    constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService) {}

    ngOnInit(): any {

        this.subscription = this.route.params.subscribe(
            (params: any) => {
                this.orderIndex = params['id'];
                this.productService.getOrder(this.orderIndex).subscribe(
                    (data: Order) => {
                        this.selectedOrder = data;
                        var parser: string = this.selectedOrder.items.toString();
                        this.items = JSON.parse(parser);
                        console.log(this.items);
                    }
                );
            }
        );

    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngAfterViewChecked() {
        window.scrollTo(0, 0);
    }

}
