import { RouterConfig } from "@angular/router";
import {OrdersStartComponent} from "./orders-start.component";
import {OrderDetailComponent} from "./orders-detail/order-detail.component";

export const ORDERS_ROUTES: RouterConfig = [
    { path: '', component: OrdersStartComponent },
    { path: ':id', component: OrderDetailComponent }
];