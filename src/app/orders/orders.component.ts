import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from "@angular/router";
import {OrdersListComponent} from "./orders-list/orders-list.component";

@Component({
    moduleId: module.id,
    selector: 'rb-orders',
    templateUrl: 'orders.component.html',
    directives: [OrdersListComponent, ROUTER_DIRECTIVES]
})
export class OrdersComponent{
}