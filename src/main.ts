import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { HTTP_PROVIDERS } from "@angular/http";
import { provideForms, disableDeprecatedForms } from "@angular/forms";

import { APP_ROUTES } from "./app/app.routes";

import { AppComponent, environment } from './app/';
import { AuthService } from "./app/shared/auth.service";
import { AuthGuard } from "./app/shared/auth.guard";
import {LocalStorageService, LocalStorageSubscriber} from 'angular2-localstorage/LocalStorageEmitter';
import {ProductService} from "./app/shared/product.service";


if (environment.production) {
  enableProdMode();
}


var appPromise = bootstrap(AppComponent, [
  HTTP_PROVIDERS,
  disableDeprecatedForms(),
  provideForms(),
  APP_ROUTES,
  AuthService,
  AuthGuard,
  ProductService
]);

/*var appPromise = bootstrap(AppComponent, [ LocalStorageService ]);*/

// register LocalStorage, this registers our change-detection.
LocalStorageSubscriber(appPromise);


