// SystemJS configuration file, see links for more information
// https://github.com/systemjs/systemjs
// https://github.com/systemjs/systemjs/blob/master/docs/config-api.md

/***********************************************************************************************
 * User Configuration.
 **********************************************************************************************/
/** Map relative paths to URLs. */
const map: any = {
  'angular2-jwt': 'vendor/angular2-jwt',
  'ng2-pagination': 'vendor/ng2-pagination',
  'angular2-image-zoom': 'vendor/angular2-image-zoom',
  'ng2-file-upload' : 'vendor/ng2-file-upload',
  'angular2-cookie': 'vendor/angular2-cookie',
  'angular2-localstorage': 'vendor/angular2-localstorage/dist',
  'angular2-modal': 'vendor/angular2-modal'
};

/** User packages configuration. */
const packages: any = {
  'angular2-jwt': 'angular2-jwt.js',
  'ng2-pagination': {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'index.js'
  },
  'angular2-image-zoom': {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'angular2-image-zoom.js'
  },
  'ng2-file-upload' : {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'ng2-file-upload.js'
  },
  'angular2-cookie' : {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'core.js'
  },
  'angular2-localstorage' : {
    format: 'cjs',
    defaultExtension: 'js',
    main: 'index.js'
  },
  'angular2-modal': {
    main: 'index.js'
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************************************
 * Everything underneath this line is managed by the CLI.
 **********************************************************************************************/
const barrels: string[] = [
  // Angular specific barrels.
  '@angular/core',
  '@angular/common',
  '@angular/compiler',
  '@angular/forms',
  '@angular/http',
  '@angular/router',
  '@angular/platform-browser',
  '@angular/platform-browser-dynamic',

  // Thirdparty barrels.
  'rxjs',

  // App specific barrels.
  'app',
  'app/shared',
  /** @cli-barrel */
];

const cliSystemConfigPackages: any = {};
barrels.forEach((barrelName: string) => {
  cliSystemConfigPackages[barrelName] = { main: 'index' };
});

/** Type declaration for ambient System. */
declare var System: any;

// Apply the CLI SystemJS configuration.
System.config({
  map: {
    '@angular': 'vendor/@angular',
    'rxjs': 'vendor/rxjs',
    'main': 'main.js'
  },
  packages: cliSystemConfigPackages
});

// Apply the user's configuration.
System.config({ map, packages });
